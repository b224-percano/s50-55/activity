import { useState , useEffect } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {

	// Checks to see if the data was successfully passed
	// console.log(props);

	// Every component receives information in a form of object.
	// console.log(typeof props);

	// Using the dot notation, access a property to retrieve the value/ data
	// console.log(props.courseProp.name)

	// Check if we can retrieve data from courseProp
	console.log(courseProp);


	/*
	The state hook for this component to be able to store its state. States are used to keep track of the information related to individual components.
		
		Syntax:
			const [getter, setter] = useState(initialGetterValue)
	*/

	const [count, setCount] = useState(0);
	// console.log(useState(0));
	const [seats, setSeats] = useState(30);

	// function enroll(){

	// 	if(seatCount == 0){
	// 		alert("No more seats.")
	// 	} else {
	// 		setCount(count + 1);
	// 		console.log("Number of Enrollees: " + count)
	// 		setSeatCount(seatCount - 1);
	// 		console.log("Number of Seats: " + seatCount)
	// 	}
	// }

	function enroll() {
		if(count < 30) {
			setCount(count + 1)
			// console.log('Enrollee: ' + count)
			setSeats(seats - 1)
			// console.log('Seats: ' + seats)
		} // else {
		// 	alert("No more seats available.")
		// }
	}

	useEffect(() => {
		if(seats === 0) {
			alert("No more seats available.")
		}
	}, [seats])


	const { name, description, price, _id } = courseProp;

	return (
		<Row>
			<Col xs={12} md={6}>
				<Card className="cardHighlight2 p-3">
				      <Card.Body>
				        <Card.Title>{name}</Card.Title>
				        <Card.Subtitle>Description:</Card.Subtitle>
				        <Card.Text>{description}</Card.Text>
				        <Card.Subtitle>Price:</Card.Subtitle>
				        <Card.Text>PhP {price}</Card.Text>
				        <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
				      </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}