import { Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function Error(){


	return(
		<Row>
			<Col className="p-5">
				<h1>Error 404 - Page Not Found</h1>
				<p>The page you are looking for cannot be found</p>
				<Button as={Link} to="/">Back to home</Button>
			</Col>
		</Row>
	)
}