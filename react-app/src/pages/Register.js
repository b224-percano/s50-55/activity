import { useEffect, useState } from "react";
import { Form, Button } from "react-bootstrap";
import { useContext } from "react";
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

export default function Register() {
  const { user, setUser } = useContext(UserContext);

  const [firstName, setFirstName] = useState(``);
  const [lastName, setLastName] = useState(``);
  const [email, setEmail] = useState(``);
  const [mobileNo, setmobileNo] = useState(``);
  const [password1, setPassword1] = useState(``);
  const [password2, setPassword2] = useState(``);
  //   State to determin whether the submit button is enabled or not
  const [isActive, setIsActive] = useState(false);

  // console.log(user);

  const navigate = useNavigate();

  //   function to simulate user registration
  function registerUser(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        // We will receive either a token or a false response.
        console.log(data);
        if (data !== true) {
          fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              password: password1,
              isAdmin: false,
              mobileNo: mobileNo,
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              // We will receive either a token or a false response.
              console.log(data);
            });

          Swal.fire({
            title: "Registration Successful",
            icon: "success",
            text: "Welcome to Zuitt!",
          });

          navigate("/login");
        } else {
          Swal.fire({
            title: "Duplicate Email Found",
            icon: "error",
            text: "Please use a different email and try again.",
          });
        }
      });

    // Clear the input fields and states
    // setEmail("");
    // setPassword1("");
    // setPassword2("");

    // alert("Thank you for registering!");
  }

  useEffect(() => {
    if (firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && mobileNo.length === 11 && password1 !== "" && password2 !== "" && password1 === password2) 
    {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, mobileNo, password1, password2]);

  return (
  	user.id !== null ? 
    <Navigate to="/courses" />
   : 
    <Form onSubmit={(e) => registerUser(e)}>
      <Form.Group className="mb-3" controlId="firstname">
        <Form.Label>First Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="First Name"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="lastname">
        <Form.Label>Last Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Last Name"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="email">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="mobileNo">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control
          type="Number"
          placeholder="Mobile Number"
          value={mobileNo}
          onChange={(e) => setmobileNo(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password1}
          onChange={(e) => setPassword1(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password2">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Verify Password"
          value={password2}
          onChange={(e) => setPassword2(e.target.value)}
          required
        />
      </Form.Group>

      {
      	isActive ? 
        <Button variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
       : 
        <Button variant="primary" type="submit" disabled id="submitBtn">
          Submit
        </Button>
  	  }
    </Form>
  );
}